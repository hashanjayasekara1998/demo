from git import Repo, cmd

#change these variables
git_url = "https://github.com/lakizuru/HostelManagementInfoSys.git"
repo_dir = r"C:\Users\lakis\OneDrive\Desktop\TestGit"
branch_name = "dev"

# cloning and checking out
# repo = Repo.clone_from(git_url, repo_dir)
# repo.git.checkout('dev')

# pulling and checking out
gitObj = cmd.Git(repo_dir)
gitObj.pull()
gitObj.checkout(branch_name)